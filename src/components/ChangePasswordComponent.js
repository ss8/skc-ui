
import SKCHttps from '../utils/SKChttp';
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';
import { Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter } from 'reactstrap'



const changePass = async (userDetails, callback) => {

    SKCHttps.post("/user/changePassword", userDetails, {
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        }
    }).then(res => {
        callback(res)
    })
        .catch((error) => {
            callback(error.response)
        });
}

const ChangePassword = (props) => {


    const [modalFlag, setModalFlag] = useState(false);
    const [message, setMessage] = useState("");
    const [modalMessage, setModalMessage] = useState("");
    const history = useHistory();

    const handleSubmit = (event) => {
        event.preventDefault();
        var userDetails = {
            username: localStorage.getItem("username"),
            oldPassword: event.target.oldPassword.value,
            newPassword: event.target.newPassword.value,
        }

        if (userDetails.newPassword === event.target.reenterPassword.value) {
            setMessage("")
            changePass(userDetails, (response) => {
                if (response.data === true) {
                    setModalMessage("Password Changed Successfully!!")
                    setModalFlag(true)
                } else {
                    setModalMessage("Password Change Failed!!  Old Password Incorrect")
                    setModalFlag(true)
                }
            })

        } else {
            setMessage("New password and Re-entered Password do not Match")
        }

    }

    const handleRedirect = () => {
        setModalFlag(false)
        history.push("/list");
    };

    return (
        <div className="container" style={{ width: '30em' }}>
            <Modal isOpen={modalFlag} >
                <ModalBody>
                    {modalMessage}
                </ModalBody>
                <ModalFooter>
                    <Button onClick={handleRedirect}>Continue</Button>

                </ModalFooter>
            </Modal>

            <Form onSubmit={handleSubmit}>
                <FormGroup >
                    <Label>Old Password</Label>
                    <Input type="password" id="oldPassword" name="oldPassword"></Input>
                </FormGroup>

                <br></br>

                <FormGroup >
                    <Label>New Password</Label>
                    <Input type="password" id="newPassword" name="newPassword"></Input>
                </FormGroup>

                <FormGroup >
                    <Label>Re-enter New Password</Label>
                    <Input type="password" id="reenterPassword" name="reenterPassword"></Input>
                </FormGroup>
                {message ? <p>{message}</p> : null}

                <br></br>

                <Button className="form-button" type="submit" value="submit" color="primary">
                    Change Password
                </Button>
            </Form>

        </div >
    )
}

export default ChangePassword;
