import SKCHttps from '../utils/SKChttp';
import React, { useState } from 'react';
import { useHistory, withRouter } from 'react-router-dom';
import { Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import CodeMirror from '@uiw/react-codemirror';
import 'codemirror/theme/blackboard.css'


const deleteDashboardCall = async (id, sshCredentials, callback) => {

	SKCHttps.delete(
		"/dashboard/delete/" + id,
		{ data: sshCredentials }
	).then(res => {
		callback(res)
	})
		.catch((error) => {
			callback(error.response)
		});
}


const DeleteDashboard = (props) => {


	const [deleteInProgress, setDeleteInProgress] = useState(false);
	const [success, setSuccess] = useState(false);
	const [failed, setFailed] = useState(false);
	const [failureMessage, setFailureMessage] = useState("");

	const [inactiveDashboardDeleteWarning, setInactiveDashboardDeleteWarning] = useState(false);
	const [sshCredentialsModal, setSshCredentialsModal] = useState(false);

	const [logsFlag, setLogsFlag] = useState(false);
	const [logsData, setLogsData] = useState("");

	const [events, setEvents] = useState("");


	const history = useHistory();

	const getEvents = () => {

		console.log("inside events ");
		// SKCHttps.defaults.baseURL
		let eventSource = new EventSource("/api/dashboard/events");

		console.log(eventSource);

		eventSource.onopen = (event) => {
			console.log("Test events opened");
		}

		eventSource.onmessage = (event) => {
			console.log(event.data);
			setEvents(event.data)
		}

		eventSource.onerror = (event) => {
			eventSource.close();
		}

	}

	const handleInactiveDashboardWarningClick = (event) => {
		setInactiveDashboardDeleteWarning(true);
		setSshCredentialsModal(false);
	}

	const handleDelete = (event) => {
		event.preventDefault();

		// isPending = !isPending;
		var Ip = props.url.replace("https://", "").split(":")[0] // Get Ip of Cluster Node

		console.log(Ip)
		var sshCredentials = {
			clusterIp: Ip,
			sshUsername: event.target.ssh_username.value,
			sshPassword: event.target.ssh_password.value
		}

		setSshCredentialsModal(false);
		setDeleteInProgress(true);

		getEvents();

		deleteDashboardCall(props.id, sshCredentials, (response) => {
			console.log("callback" + response.status)
			setDeleteInProgress(false)
			if (response.status === 200) {

				setFailed(false)
				setSuccess(true)
				console.log(response)
				setLogsData(response.data.logs)
			}
			// else if (response.status === 500) {
			else {

				setFailed(true)
				setSuccess(false)
				setFailureMessage(response.data.error)
				console.log(response)
				setLogsData(response.data.logs)
			}
		})
	}



	const deleteClick = () => {
		setInactiveDashboardDeleteWarning(false);
		setSshCredentialsModal(true);
	}

	const handleLogsClick = () => {

		setSuccess(false)
		setFailed(false)
		setLogsFlag(true)
	}

	return (
		<>
			<Modal isOpen={logsFlag} contentClassName="Modal" className="Modal-position">
				<ModalHeader toggle={() => {
					setLogsFlag(!logsFlag);
					history.push("/");
				}} >Logs</ModalHeader>
				<ModalBody >
					<CodeMirror
						value={logsData}
						options={{
							theme: "blackboard",
							readOnly: "nocursor",
						}}
						height="78vh"
					/>
				</ModalBody>
			</Modal>
			<Modal isOpen={inactiveDashboardDeleteWarning} className="container">
				<ModalHeader toggle={() => { setInactiveDashboardDeleteWarning(!inactiveDashboardDeleteWarning) }}>
					<Label className="warning">Warning!!</Label>
				</ModalHeader>
				<ModalBody>
					<p>The Dashboard Status is <b>INACTIVE</b>. Use the delete option only if -
						<ul>
							<li>
								Cluster is active and reachable but the dashboard is Inactive
							</li>
							<li>
								Cluster has been reset and you wish to remove the dashboard entry from SKC
							</li>
						</ul>

						If the dashboard is inactive due to cluster outage, wait for cluster recovery. Delete the dashboard only if
						dashboard status doesn't change to active within 5 minutes of cluster recovery.
					</p>
				</ModalBody>
				<ModalFooter>
					<Button
						className="form-button"
						onClick={deleteClick}
					>I understand and wish to proceed</Button>
				</ModalFooter>

			</Modal>
			<Modal isOpen={sshCredentialsModal} className="container">
				<ModalHeader toggle={() => { setSshCredentialsModal(!sshCredentialsModal) }}>Are you sure?</ModalHeader>
				<ModalBody>
					<p>
						This Operation will delete the Kubernetes Dashboard GUI from the Cluster.
						This will not delete/modify the Cluster.<br></br><br></br>

						Are you sure you wish to proceed?<br />
						If yes, Please enter the <b>SSH Credentials</b> for the Cluster to intiate the deletion process.
						Use the <b>same credentials</b> which were used to install the dashboard.
					</p>

					<hr></hr>

					<h4>SSH Credentials</h4>
					<Form onSubmit={handleDelete}>
						<FormGroup >
							<Label>UserName</Label>
							<Input type="text" id="ssh_username" name="ssh_username">
							</Input>
						</FormGroup>

						<FormGroup >
							<Label>Password</Label>
							<Input type="password" id="ssh_password" name="ssh_password">
							</Input>
						</FormGroup>

						<br></br>

						<Button className="form-button" type="submit" value="submit" color="primary">
							Submit
						</Button>
					</Form>
				</ModalBody>

			</Modal>
			<Modal isOpen={deleteInProgress} >
				<ModalHeader>Deleting Dashboard</ModalHeader>
				<ModalBody>
					Please wait while the dashboard is being deleted.<br></br><br></br>
					<p className="events">{' - ' + events}</p>
				</ModalBody>
			</Modal>
			<Modal isOpen={success} className="successModal">
				<ModalHeader>Success</ModalHeader>
				<ModalBody>
					Dashboard SuccessFully Uninstalled!!
				</ModalBody>
				<ModalFooter>
					<Button onClick={handleLogsClick}>View Logs</Button>
					<Button onClick={() => {
						setSuccess(false);
						history.push("/");
					}}>OK</Button>
				</ModalFooter>
			</Modal>
			<Modal isOpen={failed} className="failedModal">
				<ModalHeader>Failed</ModalHeader>
				<ModalBody>
					Dashboard Uninstallation Failed!!
					<p>{failureMessage}</p>
				</ModalBody>
				<ModalFooter>
					<Button onClick={handleLogsClick}>View Logs</Button>
					<Button onClick={() => {
						setFailed(false);
						history.push("/");
					}}>OK</Button>
				</ModalFooter>
			</Modal>
			<Button color='danger'
				onClick={
					(event) => {
						if (props.status==="DASHBOARD_INACTIVE"){
							handleInactiveDashboardWarningClick(event)
						}
						else{
							deleteClick(event)
						}
					}
				}>Delete</Button>
		</>
	);
}

export default withRouter(DeleteDashboard);