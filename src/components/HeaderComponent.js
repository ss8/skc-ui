import React, { Component } from 'react';
import { Navbar, NavbarBrand, Nav, Button, NavLink } from "reactstrap";
import '../App.css'

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isNavOpen: false
        }

        this.toggleNav = this.toggleNav.bind(this);
    }

    toggleNav() {
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }

    render() {
        return (
            <div className="header">
                <Navbar className="navbar" dark expand="md">
                    <NavbarBrand href="/">SS8 Kubernetes Controller</NavbarBrand>
                    <Nav className="nav" navbar>
                        {/* <NavItem> */}
                        <NavLink hidden={!(this.props.login === "LOGGED_IN")}
                            href="/add" style={{ color: 'Menu' }}>
                            Add Dashboard
                        </NavLink>

                        {/* </NavItem> */}

                        <NavLink hidden={!(this.props.login === "LOGGED_IN")}
                            href="/changePassword" style={{ color: 'Menu' }}>
                            Change Password
                        </NavLink>
                        <Button className="form-button" hidden={!(this.props.login === "LOGGED_IN")}
                            onClick={this.props.logout}>Logout</Button>
                    </Nav>
                </Navbar>
            </div>
        );
    }
}

export default Header;