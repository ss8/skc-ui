
import SKCHttps  from '../utils/SKChttp';
import React, { useEffect, useState } from 'react';
import { Button } from 'reactstrap';

import DeleteDashboard from './DeleteDashboardComponent';
import { useHistory } from 'react-router-dom';


const ListDashboard = () => {
    const [dashboardList, setDashboardList] = useState([]);
    const [isPending, setIsPending] = useState(true);
    const [error, setError] = useState(null);
    const [searchTerm, setSearchTerm] = useState("");
    const history= useHistory();

    const fetchDashboardList = async () => {
        try {

            const response = await SKCHttps.get("/dashboard/list");
            // console.log(response);

            if (response.status === 200) {
                const data = response.data;
                setIsPending(false);
                setError(null);
                setDashboardList(data.list_data.dashboard_list);
            }
            else {
                const err = response.data;
                setIsPending(false);
                setDashboardList([]);
                setError(err.error.message);
            }
        } catch (err) {
            console.error(err);
            setIsPending(false);
            setDashboardList([]);
            setError(err.message);
        }
    };

    useEffect(() => {
        fetchDashboardList();
    }, [isPending]);

    const redirectToDashboard = (url, token) => {
        // const final_url = url + "/?token=" + token;
        history.push({
            pathname:"/dashboard",
            state:{
                url:url
            }
        })
        window.open(url, '_blank');
    }

    const renderDashboardListTable = (dashboardList) => {
        return (
            <div className="dashboard-list">
                <table border='1' width='100%'>
                    <thead>
                        <tr>
                            <th>Dashboard Status</th>
                            <th>Cluster Name</th>
                            {/* <th>URL</th> */}
                            {/* <th>Cluster Type</th> */}
                            <th>Dashboard Created On</th>
                            <th>Dashboard Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {dashboardList.filter((val) => {
                            if (searchTerm === "") {
                                return val
                            } else if (val.name.toLowerCase().includes(searchTerm.toLowerCase())) {
                                return val
                            } else {
                                return null;
                            }

                        }).map((dashboard) => (
                            <tr key={dashboard.id} style={{ height: "50px" }}>
                                <td>
                                    {dashboard.dashboardStatus === "DASHBOARD_ACTIVE" && <span aria-label="tick" role="img">✅</span>}
                                    {dashboard.dashboardStatus === "DASHBOARD_INACTIVE" && <span aria-label="cross" role="img">❌</span>}
                                </td>
                                {/* <td>{dashboard.name}</td> */}
                                {/* <td><a href="http://10.0.157.101:5000/dashboard?token=eyJhbGciOiJSUzI1NiIsImtpZCI6IkRXV3NrallhWnphWEJldWZ5VUIzclNpeEN6TW50ZDNLRkZMdnNTaWVXLTAifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJza2MtYWRtaW4tdG9rZW4tdnFrY3MiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoic2tjLWFkbWluIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiZDJhNzMyNTktZmY5Mi00Y2U0LTk5MDktMzNlM2MyM2E2ZmQxIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmVybmV0ZXMtZGFzaGJvYXJkOnNrYy1hZG1pbiJ9.XlqFxMs_dab4cotmnriBUneKZ26vjYDf0qegnyLVExN88FLjFXbpvBrl3xlmOWHlxBHtaPW7vXHo3j8QhLnB9nM5KYgOshTnAf3OS_WCT6JjzCEdROnqDHor4Ptjdlmm7iejaunz47BcssegwDMB_k0gKnJFPtxH-fQ83Dk4xoOL9m6ogeBwGt_53PBETjEYKRO0DdyfJ3rJKt5oT4cZtV2eqLhKIzMS7xAHrGCGU3ifAYoJe_wKf2jsetuMcYj9FJZ0Y9M3Kp9am8qBZ9jEeXYcZSe6c78TjEkdfN5npD2gWkkTFlNQcYRLCL9m0qTpSvioloynmkxyzYpQjC-APQ" target="_blank" rel="noopener noreferrer">{dashboard.url}</a></td> */}
                                <td><Button color='link' onClick={() => redirectToDashboard(dashboard.url, "")} disabled={dashboard.dashboardStatus === "DASHBOARD_INACTIVE"}>{dashboard.name}</Button></td>
                                {/* <td>{dashboard.clusterType}</td> */}
                                <td>{dashboard.createdAt}</td>
                                <td>
                                    {/* <Button color='primary'>Edit</Button> */}
                                    &nbsp;
                                    <DeleteDashboard id={dashboard.id} url={dashboard.url} status={dashboard.dashboardStatus} />
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    };

    return (
        <div className="container">

            {/* Render table using a component */}
            {/* {dashboardList.length !== 0 && <DashboardList dashboardList={dashboardList} addAdminCookie={addAdminCookie} redirectToDashboard={redirectToDashboard}/>} */}

            {/* Render table using function */}
            <input
                className="searchBar"
                type="text"
                placeholder="Search....."
                onChange={(event) => {
                    setSearchTerm(event.target.value);
                }}
            />

            {/* Show Loading message until state is changed */}
            {isPending && <div>Loading...</div>}
            {dashboardList.length !== 0 && renderDashboardListTable(dashboardList)}
            {(dashboardList.length === 0 && isPending === false) ? <div>No Dashboards Found</div> : null}

            {/* If error */}
            {error && <div>{error}</div>}
        </div>
    );
}

export default ListDashboard;