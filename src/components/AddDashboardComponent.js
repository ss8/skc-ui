import SKCHttps from '../utils/SKChttp';
import React, { useState } from 'react';
import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Form, FormGroup, Input, Label, Button, Modal, ModalHeader, ModalBody, ModalFooter, Col, Row } from 'reactstrap';

import CodeMirror from '@uiw/react-codemirror';
import 'codemirror/theme/blackboard.css'
import 'codemirror/theme/oceanic-next.css'
import { Dropdown } from 'bootstrap';



const installDash = async (clusterDetails, callback) => {

    SKCHttps.post("/dashboard/install", clusterDetails, {
        headers: {
            "Content-type": "application/json; charset=UTF-8",
        }
    }).then(res => {
        callback(res)
    })
        .catch((error) => {
            callback(error.response)
        });
}

const getClusterList = async (callback) => {
    // const data = await SKCHttps.get("/dashboard/clusterList");

    SKCHttps.get("/dashboard/clusterList")
        .then(data => {
            callback(data.data.cluster_data.clusterList)
        })
        .catch(error => {
            if (error.response.status == 404) {
                callback([])
            }
        })
    // console.log(data.data)
    // callback(data.data.cluster_data.clusterList)

}


const AddDashboard = (props) => {

    const [installing, setInstalling] = useState(false);
    const [fetchingClusterList, setfetchingClusterList] = useState(true);
    const [clusterListFound, setClusterListFound] = useState(true);
    const [success, setSuccess] = useState(false);
    const [failed, setFailed] = useState(false);
    const [dashboardInstalled, setDashboardInstalled] = useState(false);
    const [failureMessage, setFailureMessage] = useState("");
    const [clusterList, setClusterList] = useState([]);
    const [clusterSource, setClusterSource] = useState("SKI");
    const [showClusterIp, setShowClusterIp] = useState(false);
    const history = useHistory();

    const [logsFlag, setLogsFlag] = useState(false);
    const [logsData, setLogsData] = useState("");

    const [events, setEvents] = useState("");



    useEffect(
        () => {
            console.log("state changed")
        },
        [installing]
    );

    useEffect(() => {
        getClusterList((list) => {
            let options = []

            if (list.length > 0) {
                list.forEach(element => {
                    const elems = element.split(":")
                    options.push(
                        <option
                            key={Math.random()}
                            value={element}
                        >
                            {elems[0]}
                            {showClusterIp ? " | " + elems[1] : ""}
                        </option>
                    )
                });
                setClusterList(options)
            } else {
                setClusterListFound(false);
                setClusterSource("Manual")
            }

            setfetchingClusterList(false)
        })

    }, [showClusterIp])

    const handleRedirect = () => {
        history.push("/list");
    };



    const getEvents = () => {

        console.log("inside events ");

        let eventSource = new EventSource("/api/dashboard/events");

        console.log(eventSource);

        eventSource.onopen = (event) => {
            console.log("Test events opened");
        }

        eventSource.onmessage = (event) => {
            setEvents(event.data)

        }

        eventSource.onerror = (event) => {
            eventSource.close();
        }

    }

    const handleSubmit = (event) => {
        event.preventDefault();
        let clusterDetails;

        if (clusterSource === "SKI") {

            let clusterInfo = event.target.name.value.split(":")

            clusterDetails = {
                clusterIp: clusterInfo[1],
                clusterName: clusterInfo[0],
                sshUsername: event.target.ssh_username.value,
                sshPassword: event.target.ssh_password.value
            }
        } else {

            let RegexIP = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;

            if (!RegexIP.test(event.target.clusterIp.value)) {
                window.alert("Please enter a valid IP address!!")
                return;
            }


            clusterDetails = {
                clusterIp: event.target.clusterIp.value,
                clusterName: event.target.clusterName.value,
                sshUsername: event.target.ssh_username.value,
                sshPassword: event.target.ssh_password.value
            }
        }


        console.log(clusterDetails)


        setInstalling(true)

        getEvents();

        installDash(clusterDetails, (response) => {
            setInstalling(false)
            if (response.status === 200) {
                setSuccess(true)
                setFailed(false)
                setDashboardInstalled(true);

                setLogsData(response.data.logs)
            }
            // else if (response.status === 500) {
            else {
                setSuccess(false)
                setFailed(true)
                setDashboardInstalled(false);
                setFailureMessage(response.data.error)

                setLogsData(response.data.logs)
            }
        })

    }

    const handleLogsClick = () => {
        setSuccess(false)
        setFailed(false)
        setLogsFlag(true)
    }

    const handleRadioClick = (event) => {
        setClusterSource(event.target.defaultValue)
    }

    const handleCheckboxClick = (event) => {
        setShowClusterIp(!showClusterIp);

        // setShowClusterIp(!showClusterIpRef.current)

    }

    return (
        <div className="container" style={{ width: '45em' }}>
            <Label className="add-dashboard-title">
                <h3>Add Dashboard</h3>
            </Label>
            <Modal isOpen={logsFlag} contentClassName="Modal" className="Modal-position">
                <ModalHeader toggle={() => {
                    setLogsFlag(!logsFlag)

                    if (dashboardInstalled) {
                        history.push("/list");
                    }
                }} >Logs</ModalHeader>
                <ModalBody >
                    <CodeMirror
                        value={logsData}
                        options={{
                            theme: "blackboard",
                            readOnly: "nocursor",
                        }}
                        height="78vh"
                    />
                </ModalBody>
            </Modal>

            <Modal isOpen={fetchingClusterList} >
                <ModalHeader>Fetching List of Clusters</ModalHeader>
                <ModalBody>
                    Fetching List of Available Clusters
                </ModalBody>
            </Modal>
            <Modal isOpen={installing} >
                <ModalHeader>Setting Up Dashboard</ModalHeader>
                <ModalBody>
                    <Label>
                        This pop up will auto close when Dashboard is Setup
                    </Label>
                    <p className="events">{'- ' + events}</p>
                </ModalBody>

            </Modal>
            <Modal isOpen={success} className="successModal">
                <ModalHeader toggle={() => {
                    setSuccess(!success)
                    if (dashboardInstalled) {
                        history.push("/");
                    }
                }}>Success</ModalHeader>
                <ModalBody>
                    Dashboard SuccessFully Installed!!
                </ModalBody>
                <ModalFooter>
                    <Button onClick={handleLogsClick}>View Logs</Button>
                    <Button onClick={handleRedirect}>Continue</Button>

                </ModalFooter>
            </Modal>
            <Modal isOpen={failed} className="failedModal">
                <ModalHeader toggle={() => { setFailed(!failed) }}>Failure</ModalHeader>
                <ModalBody>
                    Dashboard Installation Failed !!
                    <Label>{failureMessage}</Label>
                </ModalBody>
                <ModalFooter>
                    <Button onClick={handleLogsClick}>View Logs</Button>
                </ModalFooter>
            </Modal>

            <Form onSubmit={handleSubmit}>
                <Row className="cluster-input-block">
                    {/* Existing Cluster        */}
                    <FormGroup row className="cluster-input-option" hidden={!clusterListFound}>
                        <Row>
                            <Label className="cluster-input-title">
                                <Input
                                    type="radio"
                                    name="clusterSource"
                                    value="SKI"
                                    onChange={handleRadioClick}
                                    defaultChecked={true}
                                ></Input>{'  '}
                                Existing Clusters
                            </Label>
                        </Row>
                        <Row hidden={clusterSource !== "SKI"} className="cluster-input-field">
                            <Label sm={3}>Cluster Name</Label>
                            <Col sm={9}>
                                <Input type="select" id="name" name="name">
                                    {clusterList}
                                </Input>
                            </Col>
                        </Row>

                        <Row hidden={clusterSource !== "SKI"}>
                            <Col sm={3}>
                            </Col>
                            <Col sm={9}>
                                <Input
                                    type="checkbox"
                                    name="showClusterIp"
                                    id="showClusterIp"
                                    checked={showClusterIp}
                                    onChange={handleCheckboxClick}
                                /> Show cluster IP
                            </Col>
                        </Row>

                    </FormGroup>

                    {/* Manual Entry */}
                    <FormGroup row className="cluster-input-option">
                        <Row >
                            <Label className="cluster-input-title">
                                <Input
                                    type="radio"
                                    name="clusterSource"
                                    value="Manual"
                                    onChange={handleRadioClick}
                                    checked={!clusterListFound || clusterSource === "Manual"}
                                ></Input>{'  '}
                                Custom Cluster
                            </Label>
                        </Row>

                        <Row hidden={clusterSource !== "Manual"} className="cluster-input-field">
                            <Label sm={3}>Cluster IP</Label>
                            <Col sm={9}>
                                <Input
                                    type="text"
                                    id="clusterIp"
                                    name="clusterIp"
                                    placeholder="Enter API server / Controller Node IP. [example: 10.0.157.100]"
                                    required
                                    disabled={clusterSource !== "Manual"}
                                ></Input>
                            </Col>
                        </Row>

                        <Row hidden={clusterSource !== "Manual"} className="cluster-input-field">
                            <Label sm={3} >Cluster Name</Label>
                            <Col sm={9}>
                                <Input
                                    type="text"
                                    id="clusterName"
                                    name="clusterName"
                                    placeholder="Use small case alphanumeric characters [ex: k8s-cluster-1]"
                                    required
                                    disabled={clusterSource !== "Manual"}
                                ></Input>
                            </Col>

                        </Row>
                    </FormGroup>
                </Row>

                <Row className="cluster-credentials-block">
                    <Label className="cluster-credentials-title">Cluster Node SSH Credentials</Label>

                    <FormGroup row>
                        <Row className="cluster-credentials-field">
                            <Label sm={3}>UserName</Label>
                            <Col sm={9}>
                                <Input type="text" id="ssh_username" name="ssh_username" required>
                                </Input>
                            </Col>
                        </Row>

                        <Row className="cluster-credentials-field">
                            <Label sm={3}>Password</Label>
                            <Col sm={9}>
                                <Input type="password" id="ssh_password" name="ssh_password" required>
                                </Input>
                            </Col>
                        </Row>
                    </FormGroup>

                </Row>



                <Button className="form-button" type="submit" value="submit" color="primary">
                    Add Dashboard
                </Button>
                <br /><br />

            </Form>

        </div >
    );
}

export default AddDashboard;