import React from 'react'
import { withRouter } from 'react-router-dom';

const DashboardComponent = (props) => {


    return (
        <div className="dash-div">
            <iframe
                title="k8s-dashboard-iframe"
                src={props.location.state.url}
                frameborder="0"
                width="100%"
                height="100%"
            />
        </div>
    )
}

export default withRouter(DashboardComponent);
