import SKCHttps  from '../utils/SKChttp';
import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import authToken from '../utils/authToken';

class Login extends Component {


    handleLogin = (event) => {
        event.preventDefault();
        var loginDetails = {
            username: this.username,
            password: this.password
        }

        SKCHttps
            .post("/login", loginDetails, {
                headers: {
                    "Content-type": "application/json; charset=UTF-8",
                }
            })
            .then((response) => {

                if (response.headers["authorization"]) {
                    var token = response.headers["authorization"];
                    authToken.setToken(token)
                    localStorage.setItem("username", loginDetails.username)
                    this.props.setLoginState("LOGGED_IN")
                    console.log("LOGGED_IN")
                    this.props.history.push("/list")
                }
            },
                (error) => {
                    alert("Please Enter Correct Credentials")
                    authToken.removeToken();
                    localStorage.removeItem("username")
                })
            .catch(
                err => {
                    authToken.removeToken();
                    localStorage.removeItem("username")
                }

            );
    }

    render() {

        if (this.props.loggedIn === "LOGGED_IN") {
            return <Redirect to="/list" />
        }
        else {
            return (
                <div className="container" style={{ width: '30em' }}>
                    <Form onSubmit={this.handleLogin}>
                        <FormGroup >
                            <Label>Username</Label>
                            <Input type="text" id="username" name="username"
                                onChange={e => this.username = e.target.value}></Input>
                        </FormGroup>

                        <FormGroup>
                            <Label>Password</Label>
                            <Input type="password" id="password" name="password"
                                onChange={e => this.password = e.target.value}></Input>
                        </FormGroup>
                        <br></br>

                        <Button className="form-button" type="submit" value="submit" color="primary">
                            Log In
                        </Button>
                    </Form>
                </div>
            );
        }
    }
}

export default withRouter(Login);