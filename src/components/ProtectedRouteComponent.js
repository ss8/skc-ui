import React from 'react';
import { Route , Redirect} from 'react-router-dom';
import authToken from '../utils/authToken';


export const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={props => {
                if (authToken.isTokenValid()) {
                    
                    return <Component {...props} />;
                }
                else{
                    return <Redirect to={
                        {
                            pathname: "/login",
                            state: {
                                from: props.location
                            }
                        }
                    }/>
                }
            }}
        />
    );
};