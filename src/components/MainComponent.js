import React, { Component } from 'react';
import Header from './HeaderComponent';

import AddDashboard from './AddDashboardComponent';
import ListDashboard from './ListDashboardComponent';
import { Route, Switch, Redirect } from 'react-router-dom';
import { ProtectedRoute } from './ProtectedRouteComponent';
import Login from './LoginComponent';
import authToken from '../utils/authToken';

import ChangePassword from './ChangePasswordComponent';
import DashboardComponent from './DashboardComponent';



class Main extends Component {

    constructor(props) {
        super(props);

        this.state = {
            login: "NOT_LOGGED_IN"
        };

        this.setLoginState.bind(this);
        this.handleLogout.bind(this);
    }

    componentDidMount() {
        if (authToken.getToken()) {
            this.setState({
                login: "LOGGED_IN"
            })

            return <Redirect to="/list" />
        }
    }

    setLoginState = (loginStatus) => {
        this.setState({
            login: loginStatus
        });
    }

    handleLogout() {
        console.log("logout called");
        authToken.removeToken();
        this.setState({
            login: "NOT_LOGGED_IN"
        })
    }

    render() {
        return (
            <div className="wrapper">
                <Header {...this.state} logout={() => this.handleLogout()} />
                {/* {this.state.loggedIn ? null : this.Login()} */}
                <Switch >
                    <Route exact path="/login" component={() => <Login setLoginState={this.setLoginState} loggedIn={this.state.login} />} />
                    <ProtectedRoute exact path="/list" component={ListDashboard} />
                    <ProtectedRoute path="/add" component={AddDashboard} />
                    <ProtectedRoute path="/dashboard" component={DashboardComponent} />
                    <ProtectedRoute path="/changePassword" component={ChangePassword} />
                    <Redirect to="/list" />
                </Switch>
            </div>
        );
    }
}

export default Main;