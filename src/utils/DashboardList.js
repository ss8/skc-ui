import { Button } from 'reactstrap';

const DashboardList = (props) => {
	const dashboardList = props.dashboardList;
	const addAdminCookie = props.addAdminCookie;
	const redirectToDashboard = props.redirectToDashboard;

	return (
		<div className="dashboard-list">
			<table border='1' width='100%'>
				<thead>
					<tr>
						<th>Status</th>
						<th>Name</th>
						<th>URL</th>
						<th>Cluster Type</th>
						<th>Created On</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{dashboardList.map((dashboard) => (
						<tr key={dashboard.id}>
							<td>
								{dashboard.dashboardStatus === "DASHBOARD_ACTIVE" && <span aria-label="tick" role="img">✅</span>}
								{dashboard.dashboardStatus === "DASHBOARD_INACTIVE" && <span aria-label="cross" role="img">❌</span>}
							</td>
							<td>{dashboard.name}</td>
							{/* <td><a href="http://10.0.157.101:5000/dashboard?token=eyJhbGciOiJSUzI1NiIsImtpZCI6IkRXV3NrallhWnphWEJldWZ5VUIzclNpeEN6TW50ZDNLRkZMdnNTaWVXLTAifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJza2MtYWRtaW4tdG9rZW4tdnFrY3MiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoic2tjLWFkbWluIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiZDJhNzMyNTktZmY5Mi00Y2U0LTk5MDktMzNlM2MyM2E2ZmQxIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmVybmV0ZXMtZGFzaGJvYXJkOnNrYy1hZG1pbiJ9.XlqFxMs_dab4cotmnriBUneKZ26vjYDf0qegnyLVExN88FLjFXbpvBrl3xlmOWHlxBHtaPW7vXHo3j8QhLnB9nM5KYgOshTnAf3OS_WCT6JjzCEdROnqDHor4Ptjdlmm7iejaunz47BcssegwDMB_k0gKnJFPtxH-fQ83Dk4xoOL9m6ogeBwGt_53PBETjEYKRO0DdyfJ3rJKt5oT4cZtV2eqLhKIzMS7xAHrGCGU3ifAYoJe_wKf2jsetuMcYj9FJZ0Y9M3Kp9am8qBZ9jEeXYcZSe6c78TjEkdfN5npD2gWkkTFlNQcYRLCL9m0qTpSvioloynmkxyzYpQjC-APQ" target="_blank" rel="noopener noreferrer">{dashboard.url}</a></td> */}
							<td><Button color='link' onClick={() => redirectToDashboard(dashboard.url, "")}>Open in New Tab</Button></td>
							<td>{dashboard.clusterType}</td>
							<td>{dashboard.createdAt}</td>
							<td>
								<Button color='primary'>Edit</Button>
								&nbsp;
								<Button color='danger'>Delete</Button>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
}

export default DashboardList;