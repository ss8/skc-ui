import axios from "axios";
import https from "https";
import authToken from "./authToken";

// class SKChttp {
// 	skchttp = null;

// 	constructor(basePath) {

// 		var skcHttpsAgent = new https.Agent({
// 			rejectUnauthorized: false
// 		});

// 		this.skchttp = axios.create({
// 			httpsAgent: new https.Agent({  
// 			  rejectUnauthorized: false
// 			})
// 		  });


// 	}

// 	getSKChttp() {

// 		if (authToken.getToken != null) {
// 			this.skchttp.defaults.headers.common["Authorization"] = authToken.getToken();
// 		}

// 		return this.skchttp
// 	}
// }

// const instance = new SKChttp('https://10.0.157.100:8443');

// https.globalAgent.options.rejectUnauthorized=false;
process.env.NODE_TLS_REJECT_UNAUTHORIZED ='0'

const SKCHttps = axios.create({
	httpsAgent:new https.Agent({
		rejectUnauthorized:false
	}),
	// baseURL: 'https://10.0.157.100:8443',
	// baseURL:'https://localhost:8443/',
	baseURL:"/api"

})

SKCHttps.interceptors.request.use((config) => {
	if (authToken.getToken() != null) {
		config.headers.Authorization = authToken.getToken();
	}

	return config;
});


Object.freeze(SKCHttps)

export default SKCHttps;