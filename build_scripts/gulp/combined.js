// Gulp Tasks for creating docker images and tars of both ui & backend combined.

// import { series, parallel } from "gulp";
// import log from "fancy-log";
// import fs from "fs";
// import gulpGit from "gulp-git"

const { series, parallel } = require("gulp");
const log = require("fancy-log");
const gulpGit = require("gulp-git");

const git = {
	repo_name: "skc",
	checkout_branch: "development"
}

clean = () => {
	log(`Cleaning directories...`);
}

gitclone = (cb) => {
	log(`Cloning ${git.repo_name} repository to local temp directory`);
	gulpGit.clone(`https://bitbucket.org/ss8/${git.repo_name}.git`, { args: `.//${git.repo_name}/` }, (err) => {
		cb(err);
	});
	log(`Cloned ${git.repo_name} repository to local temp directory`);
}

gitcheckout = (cb) => {
	log(`Checking out to ${git.checkout_branch} branch`);
}

exports.clone = series(
	gitclone
)