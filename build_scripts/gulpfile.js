const { src, dest, series, parallel } = require('gulp');
const del = require('del');
const fs = require('fs');
const zip = require('gulp-zip');
const log = require('fancy-log');
// const webpack_stream = require('webpack-stream');
// const webpack_config = require('./webpack.config.js');
// var exec = require('child_process').exec;
const { exec } = require('child_process')


const prod_dir_name = "prod_build"
const src_dir_name = "skc-ui"

const paths = {
	prod_build: `../${prod_dir_name}`,
	react_src: `../../${src_dir_name}/build/**/*`,
	react_dist: `../${prod_dir_name}/${src_dir_name}/build`,
	zipped_file_name: "skc-ui.zip"
}

clean = () => {
	log(`Removing old files from ${prod_dir_name} directory`);
	return del(`${paths.prod_build}/**`, { force: true });
}

createProdBuildFolder = () => {
	const dir = paths.prod_build;

	log(`Creating 📁 ${prod_dir_name} folder if not exists`);

	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
		log(`Created 📁 ${prod_dir_name} folder`);
	}

	return Promise.resolve('The value is ignored');
}

buildReactCodeTask = (cb) => {
	log(`Building react code`);

	return exec(`cd ../../${src_dir_name} && yarn build`, (err, stdout, stderr) => {
		log(stdout);
		log(stderr);
		cb(err);
	});
}

copyReactCodeTask = () => {
	log(`Copying React production code into ${prod_dir_name}`);
	return src(`${paths.react_src}`).pipe(dest(`${paths.react_dist}`));
}

zippingTask = () => {
	log(`Zipping the ${prod_dir_name} folder`);
	return src(`${paths.prod_build}/**`).pipe(zip(`${paths.zipped_file_name}`)).pipe(dest(`${paths.prod_build}`));
}

buildReactProdImageTask = (cb) => {
	log(`Building Production Image for ${src_dir_name}`);
	return exec(`cd ../../${src_dir_name} && yarn prod-docker-build`, (err, stdout, stderr) => {
		log(stdout);
		log(stderr);
		cb(err);
	});
}

exports.skcdevbuild = series(
	clean,
	createProdBuildFolder,
	buildReactCodeTask,
	copyReactCodeTask,
	zippingTask
)

exports.skcprodbuild = series(buildReactProdImageTask)