#!/bin/bash

set -o pipefail -e

# Global variables
WORKING_DIR=$(cd $(dirname $0); pwd)

export SKC_HOME_DIR="$HOME/.skc"

export SKC_DIR="$SKC_HOME_DIR/skc"
export DASHBOARD_RESOURCES_DIR="$SKC_DIR/dashboard_resources"
export SKC_CONFIG_DIR="$SKC_DIR/config"
export SKC_LOG_DIR="$SKC_DIR/logs"
export SKC_DB_DIR="$SKC_DIR/db-data"

export SKC_UI_DIR="$SKC_HOME_DIR/skc-ui"
export SKC_UI_BUILD_DIR="$SKC_UI_DIR/build"
export SKC_UI_CERTS_DIR="$SKC_UI_DIR/certs"

export SKC_NGINX_CONF_DIR="/etc/nginx"
export SKC_NGINX_CONF_FILE="$SKC_NGINX_CONF_DIR/nginx.conf"
export SKC_NGINX_CONF_TEMP_FILE="$SKC_UI_DIR/nginx.conf"
export SKC_NGINX_CONTENT_DIR="/usr/share/nginx/html"

export DOCKER_IMAGES_DIR="$SKC_HOME_DIR/images"

export MARIADB_IMAGE_TAR="$DOCKER_IMAGES_DIR/mariadb.tar"
export MARIADB_IMAGE_NAME="mariadb:latest"
export MARIADB_CONTAINER_NAME="mariaDB"

NGINX_CMD="/usr/sbin/nginx"
DOCKER_CMD="/usr/bin/docker"
SYSTEMCTL_CMD="/usr/bin/systemctl"


# Include function to read yaml file
source ${WORKING_DIR}/parse_yaml.sh

helpfunction() {
	echo ""
	echo "Usage: $0 <action> -p|--password <password>"
	echo "Get Help: $0 -h|--help"
	echo -e "\nActions:"
	echo -e "\tinstall \t Installs SKC"
	echo -e "\t\t\t (Loads SKC related resources & deploys them using nginx & jre)"
	echo -e "\tuninstall \t Uninstalls SKC"
	echo -e "\t\t\t (Unloads SKC related resources & delete the deployment created by nginx & jre)"
	echo -e "\nOptions:"
	echo -e "\t -p|--password \t User Password"
	echo -e "\t -h|--help \t Help or usage"
	# exit 1
}

_info(){
    local _done="--------------"
    echo -e "\n${_done} $1 ${_done}\n"
    echo -e "${_done} On: `date` ${_done}\n"
}


# Create file if not exists
# Arguments:
# $1: Absolute Path for file (eg. /tmp/clusterlist.txt)
create_file_if_not_exists() {
	_info "Starting: Create clusterlist.txt if not exists"
	if [ ! -f $1 ]; then
		echo "File $1 does not exist.. Creating a new one!!"
		set +e
		echo > $1
		set -e
	else
		echo "File $1 already exists!!"
	fi
	_info "Done: Create clusterlist.txt if not exists"
}

load_mariadb_image() {
	_info "Starting: Loading MariaDB docker image"
	$DOCKER_CMD load < $MARIADB_IMAGE_TAR
	_info "Done: Loading MariaDB docker image"
}

run_mariadb_container() {
	_info "Starting: Run MariaDB container"
	echo "Stopping & removing any container named:- $MARIADB_CONTAINER_NAME"
	set +e
	$DOCKER_CMD stop $MARIADB_CONTAINER_NAME 2>/dev/null
	$DOCKER_CMD rm $MARIADB_CONTAINER_NAME 2>/dev/null
	set -e
	
	echo "Starting a new $MARIADB_CONTAINER_NAME container with $SKC_DB_DIR as volume mount"
	$DOCKER_CMD run -d \
		--name "$MARIADB_CONTAINER_NAME" \
		-e MARIADB_ROOT_PASSWORD=8uoNoxoS+SlWPmv8EMT+RA== \
		-e MARIADB_DATABASE=skc \
		-e MARIADB_USER=skc \
		-e MARIADB_PASSWORD=8uoNoxoS+SlWPmv8EMT+RA== \
		-p 3308:3306 \
		-v $SKC_DB_DIR:/var/lib/mysql \
		$MARIADB_IMAGE_NAME
	
	echo "Waiting for $MARIADB_CONTAINER_NAME container to accept connections..."
	sleep 20
	_info "Done: Run MariaDB container"

}


spawn_mariadb_container() {
	# Load if mariadb image exists in local registry
	_info "Starting: Spawn MariaDB container for SKC Database"

	echo "Checking if MariaDB Image exists locally.."
	local dc_ins="$DOCKER_CMD inspect --type=image $MARIADB_IMAGE_NAME"
	
	set +e
	$dc_ins 2>&1 1>/dev/null
	ret=$?
	set -e

	if [[ $ret -ne 0 ]]; then
		echo "Loading MariaDB image in local docker registry..."
		load_mariadb_image
	else
		echo "MariaDB already present in local docker registry..."
	fi

	# Run mariadb container
	echo "Spawning MariaDB Conatiner"
	run_mariadb_container
	_info "Done: Spawn MariaDB container for SKC Database"

}

start_skc_backend() {
	_info "Starting: Starting SKC Backend"

	echo "Starting SKC Backend"
	nohup java -jar $SKC_DIR/skc.jar \
		--spring.config.location="$SKC_CONFIG_DIR/application.yml","$SKC_CONFIG_DIR/application-override.yml" \
		> $SKC_LOG_DIR/skc-backend-logs.txt &

	_info "Done: Starting SKC Backend"
}

start_skc_frontend() {
	_info "Starting: Starting Nginx Server to serve SKC UI Pages"

	echo "Starting Nginx to serve SKC UI"
	# nginx
	set +e
	# pgrep -x nginx >/dev/null
	$SYSTEMCTL_CMD is-active --quiet nginx
	ret=$?
	set -e
	if [[ $ret -eq 0 ]] ; then
		echo "Nginx service is already running.."
		echo "Reloading nginx to load new configuration"
		echo "${PASSWORD}" \
			| sudo -S $NGINX_CMD -s reload
	else
		echo "Nginx service is in stopped state.."
		echo "Starting nginx service with new configuration"
		echo "${PASSWORD}" \
			| sudo -S service nginx start
	fi

	_info "Done: Starting Nginx Server to serve SKC UI Pages"
}

generate_skc_ui_certs() {
	_info "Starting: Generating SSL certificate for secure connections to Nginx Server"

	echo "Generating self-signed ssl certs for SKC UI"
	sh ${SKC_UI_DIR}/generate_ssl_certs.sh

	_info "Done: Generating SSL certificate for secure connections to Nginx Server"

}

generate_nginx_conf() {
	_info "Starting: Generating Nginx configuration file"

	echo "Generating nginx.conf using nginx.conf.template"
	envsubst '${SKC_NGINX_CONTENT_DIR} ${SKC_UI_CERTS_DIR}' \
		< $SKC_UI_DIR/nginx.conf.template \
		> $SKC_NGINX_CONF_TEMP_FILE
	
	_info "Done: Generating Nginx configuration file"
}

install() {

	# Step 1: Create skc root directory in User Home folder
	mkdir -p $SKC_HOME_DIR

	# Step 2: Copy contents to SKC Home directory & Nginx content directory
	cp -r "$WORKING_DIR"/* "$SKC_HOME_DIR"

	echo "${PASSWORD}" \
		| sudo -S \
		mkdir -p $SKC_NGINX_CONTENT_DIR
	echo "${PASSWORD}" \
		| sudo -S \
		cp -r "$SKC_UI_BUILD_DIR"/* /usr/share/nginx/html


	# Step 3: Read application.yaml & create a blank clusterlist.txt if it does not exist
	local app_yaml_dir="$SKC_CONFIG_DIR/application.yml"
	eval $(parse_yaml ${app_yaml_dir} "appconfig_")
	create_file_if_not_exists "$appconfig_ski_clusterListFilePath"

	# Step 4: Generate ssl certificates
	generate_skc_ui_certs
	
	# Step 5: Copy SKC UI nginx configuration & preserve old config if it exists
	generate_nginx_conf

	if [ -f "$SKC_NGINX_CONF_FILE" ]; then 
		echo "${PASSWORD}" \
			| sudo -S \
			mv "$SKC_NGINX_CONF_FILE" "$SKC_NGINX_CONF_FILE".old
	fi
	
	echo "${PASSWORD}" \
		| sudo -S \
		cp "$SKC_NGINX_CONF_TEMP_FILE" "$SKC_NGINX_CONF_FILE"
	
	# Remove temp created config file
	rm $SKC_NGINX_CONF_TEMP_FILE
	
	# Step 6: Create a mariaDB Container
	spawn_mariadb_container

	# Step 7: Start SKC backend
	start_skc_backend

	# Step 8: Start SKC frontend
	start_skc_frontend
}

stop_skc_backend() {
	_info "Starting: Stop SKC Backend server"

	echo "Getting SKC Backend process id.."
	local skc_pid=$(echo "${PASSWORD}" \
			| sudo -S netstat -tulnp | grep 8443 | awk '{ print substr($7, 0, length($7)-5) }')
	if [ -n "$skc_pid" ]; then
		echo "Killing SKC Backend with pid $skc_pid"
		kill -9 "$skc_pid"
	fi

	_info "Done: Stop SKC Backend server"

}

stop_mariadb_container() {
	_info "Starting: Stop MariaDB container"

	set +e
	echo "Stopping container named:= $MARIADB_CONTAINER_NAME"
	$DOCKER_CMD stop $MARIADB_CONTAINER_NAME
	if [ $? -eq 0 ]; then
		echo "Removing container named:- $MARIADB_CONTAINER_NAME"
		$DOCKER_CMD rm $MARIADB_CONTAINER_NAME
	fi
	set -e
	sleep 5

	_info "Done: Stop MariaDB container"
}

stop_skc_frontend() {
	_info "Starting: Stop Nginx server"

	echo "Stopping Nginx service.."
	echo "${PASSWORD}" \
			| sudo -S service nginx stop
	
	echo "${PASSWORD}" \
		| sudo -S \
		rm "$SKC_NGINX_CONF_FILE"
	
	echo "Restoring old nginx config (if exists .old file)..."
	if [ -f "$SKC_NGINX_CONF_FILE".old ]; then 
		echo "${PASSWORD}" \
			| sudo -S \
			mv "$SKC_NGINX_CONF_FILE".old "$SKC_NGINX_CONF_FILE"
	fi
	
	echo "Removing all HTML contents..."
	echo "${PASSWORD}" \
		| sudo -S \
		rm -rf "$SKC_NGINX_CONTENT_DIR"/*
	
	_info "Done: Stop Nginx server"
}

remove_home_folder() {
	_info "Starting: Removing SKC Home Directory $SKC_HOME_DIR"

	echo "${PASSWORD}" \
		| sudo -S \
		rm -rf "$SKC_HOME_DIR"
	
	_info "Done: Removing SKC Home Directory $SKC_HOME_DIR"

}

uninstall() {
	# Step 1: Stop SKC backend server
	stop_skc_backend

	# Step 2: Stop and remove mariadb container
	stop_mariadb_container

	# Step 3: Stop Nginx server
	stop_skc_frontend

	# Step 4: Remove SKC Home folder
	remove_home_folder
}

if [[ "$#" -lt 1 ]]; then
	echo "Please provide a action to be performed..!!!"
	helpfunction
	exit 1
fi

ACTION=$1
shift 1

if [[ "$ACTION" != "install" ]] && [[ "$ACTION" != "uninstall" ]]; then
	echo "Please provide a valid action to perform (install/uninstall)"
	helpfunction
	exit 1
fi

ARGS=$(getopt -a --options p:h --long "password:,help" -- "$@")
 
eval set -- "$ARGS"
 
while true; do
  case "$1" in
    -p|--password)
      PASSWORD="$2"
      echo "Password: $PASSWORD"
      shift 2;;
    -h|--help)
      helpfunction
	  exit
      break;;
    --)
	#   exit
      break;;
  esac
done

if [ -z "$PASSWORD" ]; then
	echo "Please provide user password!" >&2
	helpfunction
	exit 1
fi

if [ "$ACTION" == "install" ]; then
	install
elif [ "$ACTION" == "uninstall" ]; then
	uninstall
fi

set +o pipefail +e