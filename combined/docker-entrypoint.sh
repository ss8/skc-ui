#!/bin/sh

DB_TIMEOUT_SEC=60

if [[ -n "$OVERRIDE_FILES" ]] && [[ ! "$OVERRIDE_FILES" = ,* ]]; then
	OVERRIDE_FILES=",$OVERRIDE_FILES"
fi

if [[ "$1" == "container" ]]; then
	echo "Docker container selected..."
	
	echo "Starting SKC Frontend and Backend..."
	
	echo "Java Options:- $JAVA_OPTS"
	echo "Override Files:- $OVERRIDE_FILES"
	echo "Command Line Arguments:- $COMMAND_LINE_ARGUMENTS"
	
	(trap 'kill 0' SIGINT; nginx && java ${JAVA_OPTS} -jar /opt/SKC/skc/skc.jar --spring.config.location=/opt/SKC/skc/config/application.yml${OVERRIDE_FILES} ${COMMAND_LINE_ARGUMENTS})

elif [[ "$1" == "compose" ]]; then
	echo "Docker compose selected..."
	
	echo "Starting SKC Backend and Frontend..."
	
	echo "Java Options:- $JAVA_OPTS"
	echo "Override Files:- $OVERRIDE_FILES"
	echo "Command Line Arguments:- $COMMAND_LINE_ARGUMENTS"
	
	echo "Waiting for skc-maria-db service to start..."
	echo "SKC Backend will fail to start if Database is not ready within $DB_TIMEOUT_SEC sec"
	sh /wait-for skc-maria-db:3306 -t ${DB_TIMEOUT_SEC} -- nginx && java -jar ${JAVA_OPTS} /opt/SKC/skc/skc.jar --spring.config.location=/opt/SKC/skc/config/application.yml${OVERRIDE_FILES} ${COMMAND_LINE_ARGUMENTS}
fi
